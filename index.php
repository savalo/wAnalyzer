<!doctype html>
<html lang="en">
<head>
<title></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
	<style>
body {
	font-family: arial;
	background-color: #000;
	color: #0F0;
	margin: 30px 60px;
}

code {
	color: grey;
}
</style>
	<?php
	error_reporting ( 0 );
	$VER = 'v0.1';
	echo '<h1>Websitez <a href="./" style="color:orange">Analyzer</a> ' . $VER . '</h1>';
	echo '<h2>Date: ' . date ( 'r', time () ) . '</h2>';
	
	include_once ('./simple_html_dom.php');
	include_once ('./get_web_page.php');
	
	$starttime = explode ( ' ', microtime () );
	$starttime = $starttime [1] + $starttime [0];
	
	if (isset ( $_POST ['url'] )) {
		$url = $_POST ['url'];
		
		echo '<br><br>';
		echo '<hr>URL: <a href="' . $_POST ['url'] . '" target="_blank" style="color:orange">' . $_POST ['url'] . '</a>  Analysed:';
		echo '<hr>';
	} else {
		?>
		<form method="POST">
		<input type="text" name="url" value="" size="128"> <input
			type="hidden" name="next" value="1"> <input type="submit" value="Get">
	</form>
	<?php
	}
	
	if (addslashes ( $_POST ['next'] ) == '1') {
		$web_array = get_web_page ( $url );
		$web_string = implode ( ' ', $web_array );
		$html = str_get_html ( $web_string );
		
		// title
		foreach ( $html->find ( 'title' ) as $e ) {
			echo '<br><span style="color:red">Title:</span> <b>' . $e->plaintext . '</b>';
		}
		
		echo '<hr>';
		// meta description
		$meta_description = $html->find ( "meta[name='description']", 0 )->content;
		echo '<br><span style="color:red">Description:</span>' . $meta_description;
		
		echo '<hr>';
		// h1
		foreach ( $html->find ( 'h1' ) as $e ) {
			echo '<br><span style="color:red">H1:</span> <b>' . $e->plaintext . '</b>';
		}
		echo '<hr>';
		// h2
		foreach ( $html->find ( 'h2' ) as $e ) {
			echo '<br><span style="color:red">H2:</span> <b>' . $e->plaintext . '</b>';
		}
		echo '<hr>';
		// h3
		foreach ( $html->find ( 'h3' ) as $e ) {
			echo '<br><span style="color:red">H3:</span> <b>' . $e->plaintext . '</b>';
		}
		echo '<hr>';
		// a
		foreach ( $html->find ( 'a' ) as $e ) {
			echo '<br><span style="color:red">A:</span> <b>' . $e->plaintext . '</b>';
		}
		echo '<hr>';
		// ul
		foreach ( $html->find ( 'ul' ) as $e ) {
			echo '<br><span style="color:red">ul:</span> <b>' . $e->plaintext . '</b>';
		}
	}
	
	$mtime = explode ( ' ', microtime () );
	$totaltime = $mtime [0] + $mtime [1] - $starttime;
	printf ( '<br><br><hr>Page generated in %.4f sec', $totaltime );
	
	?>